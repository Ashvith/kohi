@ECHO OFF
REM Build Everything

ECHO "Building everything..."

PUSHD engine
CALL build.bat
POPD
IF %ERRORLEVELS% NEQ 0 (echo Error:%ERRORLEVELS% && exit)

PUSHD testbed
CALL build.bat
POPD
IF %ERRORLEVELS% NEQ 0 (echo Error: %ERRORLEVELS% && exit)

ECHO "All assemblies build successfully."
